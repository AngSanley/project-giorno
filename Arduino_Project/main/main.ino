#include "dht.h"
#include <SoftwareSerial.h>
#define dht_apin A0 // Analog Pin sensor is connected to

SoftwareSerial SoftSerial(2, 3); // Bluetooth softwareserial
 
dht DHT;

unsigned long previousMillis = 0;
const long interval = 2000;

bool relay0 = false;
bool relay1 = false;

void setup(){
 
  Serial.begin(9600);
  SoftSerial.begin(9600);
  delay(500);//Delay to let system boot
  delay(1000);//Wait before accessing Sensor
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
}
 
void loop(){
  unsigned long currentMillis = millis();
  SoftSerial.setTimeout(250);
  String a = SoftSerial.readStringUntil("#");
  //Serial.print(a);

  if (a.equals("getstate,all#")) {
    SoftSerial.print("0,");
    SoftSerial.println(relay0);
    SoftSerial.print("1,");
    SoftSerial.println(relay1);
  } else if (a.equals("getstate,0#")) {
    SoftSerial.print("0,");
    SoftSerial.println(relay0);
  } else if (a.equals("getstate,1#")) {
    SoftSerial.print("1,");
    SoftSerial.println(relay1);
  } else if (a.equals("toggle,0#")) {
    if (!relay0) relay0 = true;
    else relay0 = false;
    
    SoftSerial.print("0,");
    SoftSerial.println(relay0);
  } else if (a.equals("toggle,1#")) {
    if (!relay1) relay1 = true;
    else relay1 = false;
    
    SoftSerial.print("1,");
    SoftSerial.println(relay1);
  }


  if (!relay0) {
    digitalWrite(8, HIGH);
  } else {
    digitalWrite(8, LOW);
  }

  if (!relay1) {
    digitalWrite(9, HIGH);
  } else {
    digitalWrite(9, LOW);
  }
  
  if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
  
      DHT.read11(dht_apin);
      
      SoftSerial.print("3,");
      SoftSerial.println(DHT.humidity);
      SoftSerial.print("2,");
      SoftSerial.println(DHT.temperature); 
    }

 
}
