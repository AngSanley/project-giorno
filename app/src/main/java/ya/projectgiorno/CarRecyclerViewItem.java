package ya.projectgiorno;

public class CarRecyclerViewItem {

    // Save car name.
    private String carName;

    // Save car image resource id.
    private int carImageId;

    // Save switch state
    private boolean switchState;

    public CarRecyclerViewItem(String carName, int carImageId, boolean switchState) {
        this.carName = carName;
        this.carImageId = carImageId;
        this.switchState = switchState;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public int getCarImageId() {
        return carImageId;
    }

    public boolean getState() {
        return switchState;
    }

    public void setCarImageId(int carImageId) {
        this.carImageId = carImageId;
    }
}