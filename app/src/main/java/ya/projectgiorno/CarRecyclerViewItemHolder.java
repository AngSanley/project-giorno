package ya.projectgiorno;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class CarRecyclerViewItemHolder extends RecyclerView.ViewHolder {

    private TextView carTitleText = null;

    private ImageView carImageView = null;

    private Switch carSwitch = null;

    public CarRecyclerViewItemHolder(View itemView) {
        super(itemView);

        if(itemView != null) {
            carTitleText = (TextView)itemView.findViewById(R.id.switch4);

            carSwitch = (Switch) itemView.findViewById(R.id.switch4);

            carImageView = (ImageView)itemView.findViewById(R.id.card_view_image);
        }
    }

    public TextView getCarTitleText() {
        return carTitleText;
    }

    public Switch getCarSwitch() {
        return carSwitch;
    }

    public ImageView getCarImageView() {
        return carImageView;
    }
}