package ya.projectgiorno;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;

import java.util.List;

public class CarRecyclerViewDataAdapter extends RecyclerView.Adapter<CarRecyclerViewItemHolder> {

    private List<CarRecyclerViewItem> carItemList;

    public CarRecyclerViewDataAdapter(List<CarRecyclerViewItem> carItemList) {
        this.carItemList = carItemList;
    }

    @Override
    public CarRecyclerViewItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get LayoutInflater object.
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // Inflate the RecyclerView item layout xml.
        View carItemView = layoutInflater.inflate(R.layout.activity_bluetooth_cardview_layout, parent, false);

        // Get car title text view object.
        final TextView carTitleView = (TextView)carItemView.findViewById(R.id.switch4);
        // Get car image view object.
        final ImageView carImageView = (ImageView)carItemView.findViewById(R.id.card_view_image);
        // When click the image.
        carImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get car title text.
                String carTitle = carTitleView.getText().toString();
                // Create a snackbar and show it.
                //Snackbar snackbar = Snackbar.make(carImageView, "You turned " + state + " " + carTitle +" image", Snackbar.LENGTH_LONG);
                //snackbar.show();

                if (carTitle.equals("LivingRoomLight")) {
                    TestBluetoothActivity.sendMsg("toggle,0#");
                } else {
                    TestBluetoothActivity.sendMsg("toggle,1#");
                }


            }
        });

        // Create and return our custom Car Recycler View Item Holder object.
        CarRecyclerViewItemHolder ret = new CarRecyclerViewItemHolder(carItemView);
        return ret;
    }

    @Override
    public void onBindViewHolder(CarRecyclerViewItemHolder holder, int position) {
        if(carItemList!=null) {
            // Get car item dto in list.
            CarRecyclerViewItem carItem = carItemList.get(position);

            if(carItem != null) {
                // Set car item title.
                holder.getCarTitleText().setText(carItem.getCarName());
                // Set car image resource id.
                holder.getCarImageView().setImageResource(carItem.getCarImageId());
                // Set switch state
                holder.getCarSwitch().setChecked(carItem.getState());
            }
        }
    }

    @Override
    public int getItemCount() {
        int ret = 0;
        if(carItemList!=null)
        {
            ret = carItemList.size();
        }
        return ret;
    }
}