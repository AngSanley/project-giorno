package ya.projectgiorno;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        setContentView(R.layout.activity_error);
        TextView errorMsg = (TextView) findViewById(R.id.textViewError);
        errorMsg.setText(extras.getString("error"));
    }
}
