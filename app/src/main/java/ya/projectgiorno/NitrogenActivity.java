package ya.projectgiorno;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class NitrogenActivity extends AppCompatActivity {

    String server_url = "https://hahehahe.bancet.cf/control.php";
    boolean acState = false;
    boolean lightState = false;
    boolean alarmState = false;
    private SwipeRefreshLayout swipeContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nitrogen);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setState("light", R.id.switch2);
                setState("ac", R.id.switch3);
                setState("alarm", R.id.switch1);
            }
        });

        swipeContainer.setRefreshing(true);
        setState("light", R.id.switch2);
        setState("ac", R.id.switch3);
        setState("alarm", R.id.switch1);


        Switch alarmSwitch = (Switch) findViewById(R.id.switch1);
        Switch lightSwitch = (Switch) findViewById(R.id.switch2);
        Switch acSwitch = (Switch) findViewById(R.id.switch3);




        alarmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (alarmState == false) {
                        sendCommand("alarm", "1");
                    }
                } else {
                    if (alarmState == true) {
                        sendCommand("alarm", "0");
                    }
                }
            }

        });

        lightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (lightState == false) {
                        sendCommand("light", "1");
                    }
                } else {
                    if (lightState == true) {
                        sendCommand("light", "0");
                    }
                }
            }

        });

        acSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (acState == false) {
                        sendCommand("ac", "1");
                    }
                } else {
                    if (acState == true) {
                        sendCommand("ac", "0");
                    }
                }
            }

        });



    }

    void sendCommand(final String command, final String value) {
        // TODO Auto-generated method stub
        final RequestQueue requestQueue = Volley.newRequestQueue(NitrogenActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String separated[] = response.split(",");
                        if (separated[0].equals("alarm")) {
                            if (separated[1].equals("1")) {
                                alarmState = true;
                            } else {
                                alarmState = false;
                            }
                        } else if (separated[0].equals("light")) {
                            if (separated[1].equals("1")) {
                                lightState = true;
                            } else {
                                lightState = false;
                            }
                        } else if (separated[0].equals("ac")) {
                            if (separated[1].equals("1")) {
                                acState = true;
                            } else {
                                acState = false;
                            }
                        }
                        if (separated[1].equals("1")) {
                            Toast.makeText(NitrogenActivity.this, separated[0] + " enabled successfully.", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(NitrogenActivity.this, separated[0] + " disabled successfully.", Toast.LENGTH_LONG).show();
                        }

                        requestQueue.stop();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NitrogenActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
                requestQueue.stop();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("output", "plain");
                params.put(command, value);
                return params;
            }

        };
        requestQueue.add(stringRequest);
    }


    void setState(final String whatThing, final int switchID) {
        final RequestQueue requestQueue = Volley.newRequestQueue(NitrogenActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String separated[] = response.split(",");
                        Switch switchy = (Switch) findViewById(switchID);

                        if (separated[0].equals("alarm")) {
                            if (separated[1].equals("1")) {
                                alarmState = true;
                                if (whatThing.equals("alarm")) switchy.setChecked(true);

                            } else {
                                alarmState = false;
                            }
                        } else if (separated[0].equals("light")) {
                            if (separated[1].equals("1")) {
                                lightState = true;
                                if (whatThing.equals("light")) switchy.setChecked(true);
                            } else {
                                lightState = false;
                            }
                        } else if (separated[0].equals("ac")) {
                            if (separated[1].equals("1")) {
                                acState = true;
                                if (whatThing.equals("ac")) switchy.setChecked(true);
                            } else {
                                acState = false;
                            }
                        }


                        //Toast.makeText(NitrogenActivity.this, separated[1], Toast.LENGTH_LONG).show();
                        swipeContainer.setRefreshing(false);
                        requestQueue.stop();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NitrogenActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
                requestQueue.stop();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("output", "plain");
                params.put("getstate", whatThing);
                return params;
            }

        };
        requestQueue.add(stringRequest);
    }

}
