package ya.projectgiorno;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.BluetoothSerialDevice;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TestBluetoothActivity extends AppCompatActivity {
    Context context = this;
    private static SimpleBluetoothDeviceInterface deviceInterface;
    BluetoothManager bluetoothManager = BluetoothManager.getInstance();
    private String macAddr = "98:D3:41:FD:3F:33";
    String roomTemp;
    String roomHumid;
    String informationString;



    private List<CarRecyclerViewItem> carItemList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Setup our BluetoothManager
        if (bluetoothManager == null || !isBluetoothAvailable()) {
            // Bluetooth unavailable on this device :( tell the user
            Intent intent = new Intent(this, ErrorActivity.class);
            intent.putExtra("error", "Bluetooth is not available");
            startActivity(intent);
            //Toast.makeText(context, "Bluetooth not available.", Toast.LENGTH_LONG).show(); // Replace context with your context instance.
            finish();
        }

        setContentView(R.layout.activity_test_bluetooth);

        List<BluetoothDevice> pairedDevices = bluetoothManager.getPairedDevicesList();
        for (BluetoothDevice device : pairedDevices) {
            Log.d("Bluetooth", "Device name: " + device.getName());
            Log.d("Bluetooth", "Device MAC Address: " + device.getAddress());
        }
        connectDevice(macAddr);

        initializeCarItemList();
        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor ed;
                ed = prefs.edit();
                ed.putBoolean("loggedin", false);
                ed.apply();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializeCarItemList() {
        if(carItemList == null)
        {
            carItemList = new ArrayList<>();
            carItemList.add(new CarRecyclerViewItem("LivingRoomLight", R.drawable.livingroom, false));
            carItemList.add(new CarRecyclerViewItem("OfficeDeskFan", R.drawable.wallplug, false));
        }
    }

    static void sendMsg(String msg) {
        deviceInterface.sendMessage(msg);
    }

    private void updateView(){
        // Create the recyclerview.
        RecyclerView carRecyclerView = (RecyclerView)findViewById(R.id.card_view_recycler_list);
        // Create the grid layout manager with 2 columns.
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        // Set layout manager.
        carRecyclerView.setLayoutManager(gridLayoutManager);

        // Create car recycler view data adapter with car item list.
        CarRecyclerViewDataAdapter carDataAdapter = new CarRecyclerViewDataAdapter(carItemList);
        // Set data adapter.
        carRecyclerView.setAdapter(carDataAdapter);
    }

    private void connectDevice(String mac) {
        showProgress(true);
        bluetoothManager.openSerialDevice(mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnected, this::onError);
    }

    private void onConnected(BluetoothSerialDevice connectedDevice) {
        // You are now connected to this device!
        // Here you may want to retain an instance to your device:
        deviceInterface = connectedDevice.toSimpleDeviceInterface();

        // Listen to bluetooth events
        deviceInterface.setListeners(this::onMessageReceived, this::onMessageSent, this::onError);

        // Let's send a message:
        deviceInterface.sendMessage("getstate,all#");
        showProgress(false);

        informationString = "Connected MAC address: " +macAddr + "<br>";
        WebView webview = (WebView)this.findViewById(R.id.infoWebview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(informationString, "text/html; charset=utf-8", "UTF-8");
    }

    private void onMessageSent(String message) {
        // We sent a message! Handle it here.
        //Toast.makeText(context, "Sent a message! Message was: " + message, Toast.LENGTH_LONG).show(); // Replace context with your context instance.
        //Snackbar snackbar = Snackbar.make("You click " + mes +" image", Snackbar.LENGTH_LONG);
        //snackbar.show();
        //carItemList.set(1, new CarRecyclerViewItem("LivingRoomLight", R.drawable.livingroom, true));
        //updateView();

    }

    private void onMessageReceived(String message) {
        // We received a message! Handle it here.
        //Toast.makeText(context, "Received a message! Message was: " + message, Toast.LENGTH_SHORT).show(); // Replace context with your context instance.
        String msg = message;
        String split[] = msg.split(",");
        int id = Integer.parseInt(split[0]);
        if ((split[0].equals("0") || split[0].equals("1")) && split[1].equals("1")) {
            if (id == 0) {
                carItemList.set(0, new CarRecyclerViewItem("LivingRoomLight", R.drawable.livingroom, true));
                updateView();
            } else {
                carItemList.set(1, new CarRecyclerViewItem("OfficeDeskFan", R.drawable.wallplug, true));
                updateView();
            }
        } else if ((split[0].equals("0") || split[0].equals("1")) && split[1].equals("0")) {
            if (id == 0) {
                carItemList.set(0, new CarRecyclerViewItem("LivingRoomLight", R.drawable.livingroom, false));
                updateView();
            } else {
                carItemList.set(1, new CarRecyclerViewItem("OfficeDeskFan", R.drawable.wallplug, false));
                updateView();
            }
        } else if (split[0].equals("2")){
            roomTemp = split[1] + "°C<br>";
            updateInfo();
        } else if (split[0].equals("3")){
            roomHumid = split[1] + "%<br>";
            updateInfo();
        }
    }

    private void onError(Throwable error) {
        // Handle the error
    }

    private void updateInfo() {
        informationString = "Connected MAC address: " +macAddr + "<br>";
        informationString += "Room temperature: " + roomTemp;
        informationString += "Room humidity: " + roomHumid;
        WebView webview = (WebView)this.findViewById(R.id.infoWebview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(informationString, "text/html; charset=utf-8", "UTF-8");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        View mProgressView = findViewById(R.id.loadingbar);
        View mLoginFormView = findViewById(R.id.linearLayout);
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    //mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        bluetoothManager.close();
        finish();
    }

    public boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return bluetoothAdapter != null
                && bluetoothAdapter.isEnabled()
                && bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON;
    }
}
